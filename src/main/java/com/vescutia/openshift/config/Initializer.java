package com.vescutia.openshift.config;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.event.EventListener;

@Configuration
@Slf4j
public class Initializer {

    @EventListener(ApplicationReadyEvent.class)
    public void doSomethingAfterStartup() {

        log.info("App Started!");

    }

}
