package com.vescutia.openshift.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class VersionController {

    @RequestMapping(value = "/version")
    public String version() {
        return "Version 1.0.0 Blackbird!";
    }

}
